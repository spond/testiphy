Introduction
------------

Testiphy aims to
* provide standard data sets for comparing likelihoods across programs
* provide the true likelihood for specific parameter values
* help developers to implement models
* provide likelihood unit tests for a variety of programs

Testiphy does not currently test the ability of programs to maximize
the likelihood, but tests the likelihood of specific parameter values.

Install
-------

git clone git@gitlab.com:testiphy/testiphy.git

Programs
-------

Testiphy currently has some coverage for BAli-Phy, RevBayes, PAUP, raxml-ng, IQ-TREE, and PhyML.

The goal is to add tests for PAML, RAxML, BEAST1, BEAST2, phylobayes, and other packages that compute phylogenetic likelihoods.

Usage
-----

You should add testiphy to your path before running the tests.


BAli-Phy
-------

If `bali-phy` is installed, then do:

`testiphy bali-phy`

RevBayes
-------

If revbayes is installed, then do

`testiphy rb`

For revbayes you should add the line

`outputPrecision=15`

to the file `~/.RevBayes.ini`.

PAUP*
-------

If paup is installed, then do

`testiphy paup`

PhyML
-------

If phyml is installed, then do

`testiphy phyml`

The binary is assumed to be called `phyml`.

IQ-TREE
-------

If iqtree is installed, then do

`testiphy iqtree`

The binary is assumed to be called `iqtree`.

raxml-ng
-------

If raxml-ng is installed, then do

`testiphy raxml-ng`

HyPhy
-------

If hyphy is installed, then do

`testiphy hyphymp`

The binary is assumed to be called `hyphymp`.

